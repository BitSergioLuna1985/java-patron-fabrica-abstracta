### Patron de fabrica de fabricas

package AbstractFactoryPattern;

public class AbstractFactoryPattern {

	public static void main(String[] args) {
		
		//get rounded shape factory
		AbstractFactory shapeFactory =
		FactoryProducer.getFactory(false);//get an object of Shape Rounded Rectangle
		Shape shape1 = shapeFactory.getShape("RECTANGLE");
		//call draw method of Shape Rectangle
		shape1.draw();
		//get an object of Shape Rounded Square
		Shape shape2 = shapeFactory.getShape("SQUARE");
		//call draw method of Shape Square
		shape2.draw();
		//get rounded shape factory
		AbstractFactory shapeFactory1 =
		FactoryProducer.getFactory(true);
		//get an object of Shape Rectangle
		Shape shape3 = shapeFactory1.getShape("RECTANGLE");
		//call draw method of Shape Rectangle
		shape3.draw();
		//get an object of Shape Square
		Shape shape4 = shapeFactory1.getShape("SQUARE");
		//call draw method of Shape Square
		shape4.draw();
		}

}

---

package AbstractFactoryPattern;

public class FactoryProducer {
	public static AbstractFactory getFactory(boolean rounded){
		if(rounded){
		return new RoundedShapeFactory();
		}else{
		return new ShapeFactory();
		}
		}
}

---

package AbstractFactoryPattern;

public abstract class AbstractFactory {
	
	abstract Shape getShape(String shapeType);

}

---

package AbstractFactoryPattern;

public class ShapeFactory extends AbstractFactory {
	
	public Shape getShape(String shapeType){
		if(shapeType.equalsIgnoreCase("RECTANGLE")){
		return new Rectangle();
		}else if(shapeType.equalsIgnoreCase("SQUARE")){
		return new Square();
		}
		return null;
		}
}

---

package AbstractFactoryPattern;

public class RoundedShapeFactory extends AbstractFactory  {
	
		@Override
		public Shape getShape(String shapeType){
		if(shapeType.equalsIgnoreCase("RECTANGLE")){
		return new RoundedRectangle();
		}else if(shapeType.equalsIgnoreCase("SQUARE")){
		return new RoundedSquare();
		}
		return null;
		}
}

---

package AbstractFactoryPattern;

public interface Shape {
	
	public void draw();
	
}

---

package AbstractFactoryPattern;

public class Rectangle implements Shape {

	public void draw() {
		System.out.println("Inside Rectangle::draw()method.");
	}
}

---

package AbstractFactoryPattern;

public class Square implements Shape {

	public void draw() {
		System.out.println("Inside Square::draw()method.");
	}
}

---

package AbstractFactoryPattern;

public class RoundedRectangle implements Shape {

	@Override
	public void draw() {
		System.out.println("Inside RoundedRectangle::draw()method.");
		
	}
}

---

package AbstractFactoryPattern;

public class RoundedSquare implements Shape {

	@Override
	public void draw() {
		System.out.println("Inside RoundedSquare::draw()method.");
		
	}
}


